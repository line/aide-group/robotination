# robotination

Création participative de robots avec et par des enfants

<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/robotination'>https://gitlab.inria.fr/line/aide-group/robotination</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/robotination'>https://line.gitlabpages.inria.fr/aide-group/robotination</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/robotination/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/robotination/-/tree/master/src</a>
- Version `1.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/robotination.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- None

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Jessica Barnabé&nbsp; <big><a target='_blank' href='mailto:jessibarnabe@gmail.com'>&#128386;</a></big>
- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
